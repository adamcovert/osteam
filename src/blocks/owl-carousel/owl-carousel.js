$(document).ready(function () {

  $('#service-slider').owlCarousel({
    dots: false,
    responsive: {
      0: {
        items: 3,
        center: true,
        loop: true,
        autoplay: true
      },
      768: {
        items: 6
      },
      1200: {
        items: 6,
        margin: 10
      }
    }
  });

  $('#advantages-mobile-slider').owlCarousel({
    nav: true,
    navText: ['<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    items: 1,
    margin: 10,
  });

  $('#testimonials-video-slider').owlCarousel({
    video: true,
    lazyLoad: true,
    center: true,
    nav: true,
    navText: ['<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 3
      }
    }
  });

  $('#testimonials-views-slider').owlCarousel({
    dots: false,
    nav: true,
    loop: true,
    margin: 30,
    navText: ['<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2
      },
      1200: {
        items: 3
      }
    }
  });

  $('.s-our-doctors__slider').owlCarousel({
    dots: false,
    nav: true,
    navText: ['<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="5px" height="10px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    responsive: {
      0: {
        items: 1,
        margin: 20,
        center: true,
        loop: true
      },
      768: {
        items: 2
      },
      992: {
        items: 4
      }
    }
  });
});
