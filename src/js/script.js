$(document).ready(function(){

  var swiper = new Swiper('.swiper-container', {
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev',
    },
    coverflowEffect: {
      rotate: 0,
      stretch: 220,
      depth: 500,
      modifier: 1,
      slideShadows : true,
    },
  });
});