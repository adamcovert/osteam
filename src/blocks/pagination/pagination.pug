mixin pagination(mods)

  //- Param:
    mods {string} - modifiers

  //- Use:
    +pagination()
      +pagination-item('1', '/1')
      +pagination-item('...')
      +pagination-item('4', '/4')
      +pagination-item('5', '/5', true)
      +pagination-item('6', '/6')
      +pagination-item('...')
      +pagination-item('999', '/999')

  -
    var allMods = '';
    if(typeof(mods) !== 'undefined' && mods) {
      var modsList = mods.split(',');
      for (var i = 0; i < modsList.length; i++) {
        allMods = allMods + ' s-pagination--' + modsList[i].trim();
      }
    }

  .s-pagination(class=allMods, aria-label='Page navigation')&attributes(attributes)
    ul.s-pagination__list
      block



mixin pagination-item(text, href, active, mods)


  //- Declaration:
  //-   text   {string} - text
  //-   href   {string} - link
  //-   mods   {string} - modifiers
  //-   active {bool}   - active

  -
    var allMods = '';
    if(typeof(mods) !== 'undefined' && mods) {
      var modsList = mods.split(',');
      for (var i = 0; i < modsList.length; i++) {
        allMods = allMods + ' s-pagination__item--' + modsList[i].trim();
      }
    }

    if(typeof(active) !== 'undefined' && active) {
      allMods = allMods + 's-pagination__item--active';
    }

  if(typeof(href) !== 'undefined' && href)
    li.s-pagination__item(class=allMods)
      a(href=href)&attributes(attributes)!= text

  else
    li.s-pagination__item(class=allMods)
      span&attributes(attributes)!= text


mixin pagination-item-prev(href)

  li.s-pagination__item.s-pagination__item--prev
    a(href=href)
      svg(xmlns='http://www.w3.org/2000/svg', viewBox='0 0 8 8', width='8', height='8')
        path(fill='#000', fill-rule='evenodd', d='M3.483 7.917a.276.276 0 0 1-.2.083.283.283 0 0 1-.2-.483L6.6 4 3.083.483a.283.283 0 0 1 .4-.4L7.2 3.8c.11.11.11.29 0 .4L3.483 7.917zm-3 0a.276.276 0 0 1-.2.083.283.283 0 0 1-.2-.483L3.6 4 .083.483a.283.283 0 0 1 .4-.4L4.2 3.8c.11.11.11.29 0 .4L.483 7.917z')

mixin pagination-item-next(href)

  li.s-pagination__item.s-pagination__item--next
    a(href=href)
      svg(xmlns='http://www.w3.org/2000/svg', viewBox='0 0 8 8', width='8', height='8')
        path(fill='#000', fill-rule='evenodd', d='M3.483 7.917a.276.276 0 0 1-.2.083.283.283 0 0 1-.2-.483L6.6 4 3.083.483a.283.283 0 0 1 .4-.4L7.2 3.8c.11.11.11.29 0 .4L3.483 7.917zm-3 0a.276.276 0 0 1-.2.083.283.283 0 0 1-.2-.483L3.6 4 .083.483a.283.283 0 0 1 .4-.4L4.2 3.8c.11.11.11.29 0 .4L.483 7.917z')